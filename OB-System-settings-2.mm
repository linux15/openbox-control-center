<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1434416169429" ID="ID_1221899087" MODIFIED="1436697846600" STYLE="bubble" TEXT="Openbox System Settings">
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1434416369985" ID="ID_237206568" MODIFIED="1436697766331" POSITION="right" STYLE="bubble" TEXT="Tools">
<node CREATED="1434416390078" ID="ID_1108751533" MODIFIED="1436695988946" STYLE="bubble" TEXT="Desktop background">
<node CREATED="1436694924266" ID="ID_1691888182" MODIFIED="1436695072947" TEXT="pcmanfm --desktop-pref "/>
<node CREATED="1436694874835" ID="ID_464013262" MODIFIED="1436694901643" TEXT="or feh simple bg"/>
<node CREATED="1436694908969" ID="ID_1164650353" MODIFIED="1436694920322" TEXT="or feh random bg"/>
<node CREATED="1434416536758" ID="ID_579688165" MODIFIED="1436694850963" STYLE="bubble" TEXT="or nitrogen"/>
</node>
<node CREATED="1434416580766" ID="ID_413768208" MODIFIED="1436696462010" STYLE="bubble" TEXT="Screen resolution">
<node CREATED="1434416592568" ID="ID_883713463" MODIFIED="1434418224876" STYLE="bubble" TEXT="ARandR"/>
</node>
<node CREATED="1434416646407" ID="ID_1748055527" MODIFIED="1436696515890" STYLE="bubble" TEXT="Openbox Menu">
<node CREATED="1434416656328" ID="ID_1712042607" MODIFIED="1436696535561" STYLE="bubble" TEXT="obmenu (needs translation)"/>
</node>
<node CREATED="1434416686855" ID="ID_73898901" MODIFIED="1436696568369" STYLE="bubble" TEXT="Openbox Look-and-feel">
<node CREATED="1434416726351" ID="ID_1308804703" MODIFIED="1436696739863" STYLE="bubble" TEXT="obconf --tab 1 : choose window theme"/>
<node CREATED="1436696711732" ID="ID_1376121247" MODIFIED="1436696789231" TEXT="obconf --tab 2 : choose fonts for Openbox"/>
<node CREATED="1436696832618" ID="ID_1870196257" MODIFIED="1436696906750" TEXT="obconf --tab 3 : some windows preferences"/>
<node CREATED="1436696884309" ID="ID_898931399" MODIFIED="1436696896702" TEXT="obconf --tab 4 : move and resize windows"/>
<node CREATED="1436696923305" ID="ID_1501712595" MODIFIED="1436696980845" TEXT="obconf --tab 5 : mouse behavior"/>
<node CREATED="1436696956382" ID="ID_1813900173" MODIFIED="1436696971933" TEXT="obconf --tab 6 : number desktops"/>
<node CREATED="1436697022740" ID="ID_1710320312" MODIFIED="1436697037660" TEXT="obconf --tab 7 : margins of the screen"/>
<node CREATED="1436697063098" ID="ID_1822724025" MODIFIED="1436697091388" TEXT="obconf --tab 8 : docks configuration for dockapps"/>
</node>
<node CREATED="1436695155745" ID="ID_1240370348" MODIFIED="1436695169653" TEXT="Lxpanel">
<node CREATED="1436695236095" ID="ID_386708952" MODIFIED="1436695243474" TEXT="lxpanelctl config"/>
</node>
<node CREATED="1434416784480" ID="ID_1938063668" MODIFIED="1436697602511" STYLE="bubble" TEXT="tint2 panel">
<node CREATED="1434416793296" ID="ID_825338892" MODIFIED="1434418224876" STYLE="bubble" TEXT="tint2-conf"/>
</node>
<node CREATED="1436697447089" ID="ID_29203233" MODIFIED="1436697507879" TEXT="wbar (light launch bar)">
<node CREATED="1436697465644" ID="ID_1282642588" MODIFIED="1436697479564" TEXT="wbar-config"/>
</node>
<node CREATED="1436697629902" ID="ID_35337709" MODIFIED="1436697647198" TEXT="cairo-dock (less light launch bar)"/>
<node CREATED="1436697612337" ID="ID_1283488986" MODIFIED="1436697625938" TEXT="docky (needs testing"/>
<node CREATED="1436697658353" ID="ID_1934928785" MODIFIED="1436697703149" TEXT="there might be more to be listed&#x2026;"/>
<node CREATED="1434416848263" ID="ID_1516567920" MODIFIED="1436697717930" STYLE="bubble" TEXT="Fonts">
<node CREATED="1434416863367" ID="ID_532578670" MODIFIED="1434418224876" STYLE="bubble" TEXT="script ???">
<node CREATED="1436695359182" ID="ID_963453524" MODIFIED="1436695416989" TEXT="pcmanfm --desktop-pref ; desktop fonts"/>
<node CREATED="1436695420811" ID="ID_269552036" MODIFIED="1436695445082" TEXT="lxappearance : most fonts"/>
<node CREATED="1436695450959" ID="ID_312889840" MODIFIED="1436695678457" TEXT="qt4-qtconfig : other fonts"/>
<node CREATED="1436695807827" ID="ID_461193822" MODIFIED="1436695818759" TEXT="obconf --tab 2 : openbox menu fonts"/>
</node>
</node>
<node CREATED="1434416918207" ID="ID_176455295" MODIFIED="1436697782937" STYLE="bubble" TEXT="Network">
<node CREATED="1434416929535" ID="ID_1054259159" MODIFIED="1434418224876" STYLE="bubble" TEXT="wicd / wifi-radar / script ???"/>
</node>
<node CREATED="1434417289199" ID="ID_1213877863" MODIFIED="1436697791039" STYLE="bubble" TEXT="Keyboard">
<node CREATED="1434417298151" ID="ID_235500977" MODIFIED="1436699437179" STYLE="bubble" TEXT="desktop file (keyboard-configuration.desktop) &#x2192; ">
<node CREATED="1436699328419" ID="ID_241730034" MODIFIED="1436699463796" TEXT="[Desktop Entry] &#xa;Type=Application &#xa;Version=1.0 &#xa;Name=Keyboard layout &#xa;Name[fr]=Mappage clavier &#xa;Description=System wide configuration for the keyboard &#xa;Description[fr]=Configuration du clavier pour tout le syst&#xe8;me &#xa;Exec=gksu -S &quot;x-terminal-emulator -m -e &apos;dpkg-reconfigure keyboard-configuration&apos;&quot; Categories=Settings; &#xa;Terminal=false &#xa;Encoding=UTF-8 &#xa;Icon=xterm-color_48x48 &#xa;&#xa;"/>
</node>
</node>
<node CREATED="1434417564327" ID="ID_1148659808" MODIFIED="1436700083304" STYLE="bubble" TEXT="Conky *optional*">
<node CREATED="1434417602080" ID="ID_224636488" MODIFIED="1436699670454" STYLE="bubble" TEXT="conkywizard or Conky Manager ( http://xmodulo.com/gui-based-conky-config-tool.html )"/>
</node>
<node CREATED="1434417616223" ID="ID_158652863" MODIFIED="1436699695524" STYLE="bubble" TEXT="Startup">
<node CREATED="1434417630543" ID="ID_794350156" MODIFIED="1436699889604" STYLE="bubble" TEXT="with xdg-autostart (some changes ongoing about it) all we need is adding desktop files from /usr/share/applications to $HOME/.config/autostart (how to create a gui for that is to be defined - just think the pcmanfm /usr/share/applications can be opened and the icons dragged and dropped from there : maybe a lead?)&#xa;"/>
</node>
<node CREATED="1434417926431" ID="ID_1026765327" MODIFIED="1434418224877" STYLE="bubble" TEXT="QT">
<node CREATED="1434417934520" ID="ID_97046824" MODIFIED="1434418224877" STYLE="bubble" TEXT="lxqt-config"/>
</node>
<node CREATED="1434418661935" ID="ID_184375778" MODIFIED="1436698234094" TEXT="Screen locker">
<node CREATED="1436698118464" ID="ID_199743214" MODIFIED="1436698171349" TEXT="light-locker"/>
<node CREATED="1436698175466" ID="ID_569413031" MODIFIED="1436698182140" TEXT="or mate-screensaver"/>
<node CREATED="1436698186417" ID="ID_1512517885" MODIFIED="1436698194659" TEXT="or cinnamon-screensaver"/>
<node CREATED="1434418677017" ID="ID_348997861" MODIFIED="1436698209409" TEXT="or i3lock"/>
</node>
</node>
<node CREATED="1434417794799" ID="ID_630542187" MODIFIED="1434418224877" POSITION="left" STYLE="bubble" TEXT="Langage Conception"/>
</node>
</map>
